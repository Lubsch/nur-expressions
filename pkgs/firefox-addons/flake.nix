{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      mapSystems = f: builtins.mapAttrs f nixpkgs.legacyPackages;
      addonsFromPkgs = pkgs: import ./. { inherit (pkgs) fetchurl lib stdenv; };
    in {
      lib = mapSystems (_: pkgs: { inherit (addonsFromPkgs pkgs) buildFirefoxXpiAddon; });
      packages = mapSystems (_: pkgs:
        nixpkgs.lib.filterAttrs (name: _: name != "buildFirefoxXpiAddon")
        (addonsFromPkgs pkgs));
    };
}
